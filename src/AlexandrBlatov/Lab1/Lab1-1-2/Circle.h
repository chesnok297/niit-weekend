#ifndef _CIRCLE_H
#define _CIRCLE_H

class Circle
{
private:
	double Radius ;			// ������
	double Ference ;		// ������ ����������
	double Area ;			// ������� �����
	const double PI = 3.14159;
public:

	void setRadius(double Rad);
	void setFerence(double Fer);
	void setArea(double Ar);
	double getRadius() const;
	double getFerence() const;
	double getArea() const;
};


#endif
