﻿#include "Automat.h"
#include <string>
#include <iostream>
#include <sstream>
#include <Windows.h>

using namespace std;

string Automat::Menu[7] = {			"Чай (зеленый) ", "Чай (черный) ",
                                    "Кофе-капучино ", "Кофе-латте ",
                                    "Кофе-со сливками ", "Вода-с газом ", "Вода-без газа " };
string Automat::Price_str[7] = {	"- 25", "- 23",
									"- 35", "- 40",
									"- 30", "- 15", "- 10" };
int Automat::Price[7] =		{		25, 23,
									35, 40,
									30, 15, 10};


void Automat::coin(int coin)			//занесение денег на счёт пользователем;
{

    if (coin < 0)
		throw 3;

    if (Stat == 2 || Stat == 3 || Stat == 4)
	{
        Stat = 3;
		Change = 0;
		Account = Account + coin;
		str_Account = intTOstr(Account);
	}
    else if (Stat == 5)
        throw 1;
    else if (Stat == 1)
		throw 2;
	
}

string Automat::getMenu() const	//отображение меню с напитками и ценами для пользователя;
{	
    string FullMenu;
	
    for (int i = 0; i < 7; ++i)

        FullMenu = FullMenu + Menu[i] + Price_str[i] + " \n";
	
	return FullMenu;
}
string Automat::getState() const			//отображение текущего состояния для пользователя;
{
	switch (Stat)
	{
    case 2:
		if (Change == 0)
			return "Автомат готов к работе";
		else
		{
			return "Ваша сдача - " + str_Change + ". Автомат готов к работе.";
		}
	break;
    case 3:
		if (FlagChoice == false)
		{
			return "Автомат находится в режиме приема денег, на счету - " + str_Account + " руб.";
		}
		else
		{
			string str_NoPr = intTOstr((Price[Choice] - Account));   // приводим "Недостачу" к типу string
            return "Hа счету - " + str_Account + " руб.\n" + "Для приобретения " + Menu[Choice] + " необходимо внести на счет - " + str_NoPr + " руб.";
		}
	break;

    case 4:
			return "Автомат находится в режиме проверки наличности, на счету - " + str_Account + " руб." + '\n' + "Стоимость выбранного напитка " + Price_str[Choice];
	break;

    case 5:
		if (FlagChoice == false)
		{
			if (Change == 0)
				return "Автомат готовит " + Menu[Choice] + ". Подождите минутку, пожалуйста!";
			else
			{
				return "Автомат готовит " + Menu[Choice] + ".Подождите минутку, пожалуйста!"+ '\n' +"Ваша сдача - " + str_Change + " руб.";
			}
		}
		else
		{
            return "Hа счету - " + str_Account + " руб.\n" +"Вы выбрали: " + Menu[Choice] + ", стоимость выбранного напитка " + Price_str[Choice] + ".";
		}
	break;

    case 1:
		return "Автомат выключен";
	break;
	}
	return 0;
}

void Automat::choice(int n = 1) //выбор напитка пользователем;
{	
	if (n <= 0 || n > 6)
		throw 4;

  //  Stat = 5; //5
	Choice = n-1;
	FlagChoice = true;
}
void Automat::check()//проверка наличия необходимой суммы;
{
	if (Price[Choice] > Account)
	{
        Stat = 3;
		
	}
	else if (Price[Choice] < Account)
	{
        Stat = 4;
		Change = Account - Price[Choice];
		str_Change = intTOstr(Change);
	}
	else if (Price[Choice] == Account)
    {Stat = 4;
        Change = 0;
        str_Change = intTOstr(Change);
    }
}
void Automat::cancel()				//отмена сеанса обслуживания пользователем;
{
    Stat = 2;
		
	if (Price[Choice] > Account)
	{
		Change = Account;	//Сдачу возвращает getState()
		str_Change = intTOstr(Change);
		Account = 0;
		str_Account = "0";
		Choice = 0;
		FlagChoice = false;
	}
	else
	{
        Change = Account;
        str_Change = intTOstr(Change);
		Account = 0;
		str_Account = "0";
		Choice = 0;
		FlagChoice = false;
	}
}
void Automat::cook()               //имитация процесса приготовления напитка;
{
    Stat = 5;
	FlagChoice = false;
//	Sleep(5000);
}
void Automat::finish()				       //завершение обслуживания пользователя.
{
    Stat = 2;
	Change = 0;
	str_Change = "0";
	Account = 0;
	str_Account = "0";
	Choice = 0;
	FlagChoice = false;
}
