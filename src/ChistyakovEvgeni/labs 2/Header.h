#ifndef _HEADER_H_ 
#define _HEADER_H_
#include<string>
using namespace std;
enum State { Off, Wait, Accept, Check, Cook }; //состояния автомата
class Automat
{
private:
	int cash;                   //текущая сумма внесенная в автомат
	int state;                  //текущее состояние автомата
	static string menu[5];
	static int prices[5];
	static string str_price[5];
	void cook();                //имитация приготовления напитка
public:
	Automat()
	{
		cash = 0;
		state = Off;
	}
	~Automat(){}
	string get_menu();
	void On();                 //включение автомата
	void Of();
	void coin(int);            //занесени денег на счет
	int choise(string);        //выбор напитка
	int check(int);            //проверка наличности
	int cancel();              //возврат
	void finish();
};
#endif
