#include "Header.h"
#include<iostream>
using namespace std;

int main()
{
	Circle first;
	int choose;
	double enter;
	bool exit = true;

	while (exit)
	{
		choose = 0;
		first.message_menu();
		cin >> choose;
		switch (choose)
		{
		case 1:
			cout << "Enter radius of circle:" << endl;
			cin >> enter;
			first.reset();

			first.set_Radius(enter);
			break;
		case 2:
			cout << "Enter ference of circle:" << endl;
			cin >> enter;
			first.reset();
			first.set_Ference(enter);
			break;
		case 3:
			cout << "Enter area of circle:" << endl;
			cin >> enter;
			first.reset();
			first.set_Area(enter);
			break;
		case 4:
			cout << "Full information about circle:" << endl;
			cout << "Radius: " << first.get_Radius() << endl;
			cout << "Ference: " << first.get_Ference() << endl;
			cout << "Area: " << first.get_Area() << endl;
			break;
		case 5:
			exit = false;
			break;
		default:
			cout << "Error,please try again:" << endl;
			break;
		}
	}
	return 0;
}