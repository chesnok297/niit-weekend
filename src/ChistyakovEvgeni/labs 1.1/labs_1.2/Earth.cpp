#include"Header.h"
#include<iostream>
#define _USE_MATH_DEFINES
#include<math.h>

void Circle::set_Ference(double ference_)
{
	Ference = ference_; 
	calculation_area();
	calculation_radius();
}
void Circle::set_Radius(double radius_)
{ 
	Radius = radius_; 
	calculation_area();
	calculation_ference();
}
void Circle::set_Area(double area_)
 { 
 	Area = area_; 
	calculation_ference();
	calculation_radius();
 }

double Circle::get_Radius()const { return Radius; }
double Circle::get_Ference()const { return Ference; }
double Circle::get_Area()const { return Area; }

void Circle::reset()
{
	Ference = 0;
	Area = 0;
	Radius = 0;
}

void Circle::calculation_ference()//ïåðåñ÷åò äëèíû îêðóæíîñòè
{
	if (Radius)//åñëè èçâåñòåí ðàäèóñ
	{
		Ference = 2 *M_PI*Radius;
	}
	else if (Area)//åñëè èçâåñòíà ïëîùàäü
	{
		Ference = sqrt(4 *M_PI*Area);
	}
}
void Circle::calculation_area()//ïåðåñ÷åò ïëîùàäè êðóãà
{
	if (Radius)
	{
		Area =M_PI*Radius*Radius;
	}
	else if (Ference)
	{
		Area = (pow(Ference, 2)) / (4 *M_PI);
	}
}
void Circle::calculation_radius()//ïåðåñ÷åò ðàäèóñà
{
	if (Ference)
	{
		Radius = Ference / (2 *M_PI);
	}
	else if (Area)
	{
		Radius = sqrt(Area /M_PI);
	}
}