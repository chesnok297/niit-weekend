#include "Counter.h"

void Counter::inc() {
    value++;
}
void Counter::dec() {
    value--;
}
void Counter::reset() {
    value=0;
}
int Counter::get() const {
    return value;
}