#pragma once
#include <iostream>

class CircleDemo
{

public:
	CircleDemo();
	
	void setRadius(double radius);
	void setFerence(double ference );
	void setArea(double area);

	// accessors
	inline double getRadius() const
	{ return _radius; }

	inline double getFerence() const
	{ return _ference; }

	inline double getArea() const
	{ return _area; }

private:
    double _radius;
	double _ference;
	double _area;
	double const PI = 3.14;
};
